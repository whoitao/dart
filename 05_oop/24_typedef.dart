typedef MathOperation(int a, int b);

// 声明加法运算
add(int a, int b) {
  print('加法运算：'+(a+b).toString());
  return a+b;
}

// 声明减法运算
sub(int a, int b) {
  print('减法运算：'+(a-b).toString());
  return a-b;
}

add3(int a, int b, int c) {
  print('加法运算3：'+(a+b+c).toString());
  return a+b+c;
}

// 计算器
calculator(int a, int b, MathOperation op) {
  print('计算器：');
  op(a, b);
}

main() {
  print(add is MathOperation);
  print(add is Function);
  print(sub is MathOperation);
  print(sub is Function);
  print(add3 is MathOperation);
  print(add3 is Function);

  MathOperation op = add;
  op(20, 10);

  op = sub;
  op(20, 10);

  calculator(8, 5, add);
  calculator(8, 5, sub);
}