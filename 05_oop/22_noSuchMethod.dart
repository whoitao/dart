class Person {
  say() {
    print('Say something');
  }

  @override
  noSuchMethod(Invocation invocation) {
    print('未定义的方法');
    // TODO: implement noSuchMethod
    return super.noSuchMethod(invocation);
  }
}

main() {
  dynamic p = Person();
  print(p.say());
  print(p.study());
}