test() {
  return Future.delayed(Duration(seconds: 2), () {
    print('Future.delayed');
    return 123;
  });
}

main() async {
  print('start');

  // 等待 test 执行完成
  await test();

  print('end');
}