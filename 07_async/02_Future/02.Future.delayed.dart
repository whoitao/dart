main() {
  Future.delayed(
    Duration(seconds: 2),
    () {
      return 123;
      // throw Error();
    }
  ).then((value) {
    print(value);
  }).catchError(
    (err) {
      print('===========');
      print(err);
    },
    test: (error) => error.runtimeType == String,
  ).whenComplete(() {
    print('All Completed');
  });
}