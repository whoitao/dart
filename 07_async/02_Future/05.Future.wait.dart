main() {
  // 等待所有异步任务完成，并收集所有异步任务的结果
  Future.wait([
    Future.delayed(Duration(seconds: 4)).then((value) {
      print('Future 1');
      return 1;
    }),
    Future.delayed(Duration(seconds: 2)).then((value) {
      print('Future 2');
      return 2;
    }),
    Future.delayed(Duration(seconds: 3)).then((value) {
      print('Future 3');
      return 3;
    }),
  ]).then((value) {
    print('所有 Future 的执行结果是：'+value.toString());
  });
}