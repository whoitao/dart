main() {
  // 创建 Future 实例
  final f = Future(() {
    print('Create the future');
    return 123;
  });

  print(f);

  f.then((value) => print(value));

  print('Done with main');
}