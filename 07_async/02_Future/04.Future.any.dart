main() {
  print('start');
  // 返回最先完成的异步任务
  Future.any([
    Future.delayed(Duration(seconds: 4)).then((value) => 1),
    Future.delayed(Duration(seconds: 2)).then((value) => 2),
    Future.delayed(Duration(seconds: 3)).then((value) => 3),
  ]).then((value) => print('多个 Future 的最快的返回结果是：'+value.toString()));

  print('end');
}