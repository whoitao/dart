main() {
  Duration interval = Duration(seconds: 1);

  Stream<int> streamData = Stream<int>.periodic(interval, (data) => data);
  
  streamData.takeWhile((element) {
    // print('takeWhile -> $element');
    return element <= 5;
  }).where((data) {
    // print('where -> $data');
    return data % 2 == 0; // 返回偶数
  }).listen((event) {
    print('-- $event');
  }).onDone(() {
    print('onDone 结束');
  });
}