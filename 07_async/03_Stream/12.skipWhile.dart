main() {
  Duration interval = Duration(seconds: 1);

  Stream<int> streamData = Stream<int>.periodic(interval, (data) => data);

  streamData.takeWhile((element) {
    // print('takeWhile: $element');
    return element <= 6;
  }).skipWhile((element) {
    print('skipWhile: $element');
    return element <= 3;
  }).listen((event) {
    print('--: $event');
  }).onDone(() {
    print('onDone 结束');
  });
}