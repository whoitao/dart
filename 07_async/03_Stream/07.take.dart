main() {
  Duration interval = Duration(seconds: 1);
  
  Stream<int> streamData = Stream<int>.periodic(interval, (data) => data);
  streamData.take(5).listen((event) {
    print('Stream.periodic -> $event');
  }).onDone(() {
    print('Stream.periodic -> done 结束');
  });
}