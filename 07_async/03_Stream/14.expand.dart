main() {
  Duration interval = Duration(seconds: 1);

  Stream<int> streamData = Stream<int>.periodic(interval, (data) => data + 1);

  streamData.takeWhile((element) {
    // print('takeWhile -> $element');
    return element <= 3;
  }).expand((element) {
    print('expand -> $element -> ${element * 10} -> ${element * 100}');
    return [element, element * 10, element * 100];
  }).listen((event) {
    print('--: $event');
  }).onDone(() {
    print('onDone 结束');
  });
}