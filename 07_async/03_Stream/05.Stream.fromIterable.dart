Future<String> getData() {
  return Future.delayed(Duration(seconds: 2), () {
    return '当前时间：${DateTime.now()}';
  });
}

main() {
  // var data = [getData(), getData(), getData(), getData()];
  var data = [1, 2, 'hello', true, null];
  Stream.fromIterable(data)
    .listen((event) {print('Stream.fromIterable : $event');})
    .onDone(() {print('Stream.fromIterable done');});
}