main() {
  Duration interval = Duration(seconds: 1);

  Stream<int> streamData = Stream<int>.periodic(interval, (data) => data);

  streamData.takeWhile((element) {
    // print('takeWhile: $element');
    return element <= 6;
  }).skip(2).listen((event) {
    print('--: $event');
  }).onDone(() {
    print('onDone 结束');
  });
}