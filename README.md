# Dart 教程

#### 介绍
Dart Tutorial

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 视频目录

1.  概述
2.  基础环境
3.  Windows 环境搭建
4.  Mac 环境搭建
5.  初始化项目
6.  Dart 概述
7.  Dart 语法基础
8.  数据类型_Number
9.  数据类型_String
10.  数据类型_Boolean
11.  数据类型_List（上）
12.  数据类型_List（下）
13.  数据类型_Set
14.  数据类型_Map
15.  数据类型_其他
16.  运算符
17.  声明函数
18.  函数参数
19.  作用域和闭包
20.  异步函数
21.  类
22.  普通构造函数
23.  命名构造函数
24.  常量构造函数
25.  工厂构造函数
26.  访问修饰
27.  Getter 和 Setter
28.  初始化列表
29.  static
30.  元数据
31.  继承
32.  抽象类
33.  接口
34.  混入
35.  泛型简介
36.  泛型函数
37.  泛型类
38.  泛型接口
39.  泛型类型限制
40.  枚举
41.  库与生态简介
42.  自定义库
43.  系统库的引入
44.  引入部分库
45.  延迟引入
46.  part 与 part of
47.  系统库
48.  第三方库
49.  
50.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
